# Installation
- Provide Splunk admin interface page for updating existing keys within UI
- Allow tuning input timer from UI and/or conf file?
- Maybe set default time to 0?
- Is there a smart way to distinguish between APIs' data points with nicenames?
- how do we redirect from setup back to app home page?

# Data and Runtime
- Ask Duo why:
	- Source country is not there?
	- IP Address is set to 0.0.0.0 for ASA VPN integration. why?
	- Why "username" for Admins is a human readable name and not a user-id.  Or why not send both for all event-types?
	- can we log a friendly name for host instead of the API hostname which is kind of useless as-is w/o a lookup table?
- Is `_internal` index the best spot for debugging / runtime logs?
- add users and devices lookup tables as scripted inputs > lookup tables
