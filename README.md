# About

This utility leverages the Duo Security API (https://www.duosecurity.com/docs) to consume both the admin and authentication logs, and write out Duo Security logs into Splunk. Inspiration from:
- duo_log_grabber (https://github.com/libresec/duo_log_grabber)
- Duo's own examples (https://github.com/duosecurity/duo_client_python).

# Installation

1. Request Duo Security Admin API access
    https://www.duosecurity.com/docs/adminapi
2. Clone BitBucket repo and install the app on your Splunk Search Head
    https://bitbucket.org/asecurityteam/duo-log-grabber
3. Install the TA
	- If you are running the script on a Heavy Forwarder, clone the duo-security-app directory onto that Forwader as a separate app.
	- If running on the Search Head, clone that same duo-security-app directory into the Search Head's app directory (`~/etc/apps/duo-security-app`)
4. Launch the app in the UI and configure the API keys.  Hit "save" from the setup screen.
	- To add additional API keys, simply add them in with the same setup screen in sequence.
	- To remove additional API keys, delete the matching lines from your `~/local/app.conf` file.

# Notes

- Depending on Duo's rate-limitng and your script settings, it might take a while to "backfill" data from your Duo installation.
- If you change the destination index value, make sure to update your savedsearches.conf and dashboard XML files to fix the search strings.
- There is no method to update `ca_certs.pem` programmatically.  Not even sure what this is for, but you'll need to tweak it manually if you know what you're doing.

# Dependencies

The following modules are included:

- duo_client
	- https://github.com/duosecurity/duo_client_python
