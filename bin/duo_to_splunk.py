#!/usr/bin/python
import ConfigParser
import optparse
import os
import sys
import time
import socket
import json
import logging
import logging.handlers
import urllib
import splunk
import splunk.entity as entity
import splunk.rest as rest
import splunk.input as input


# import Duo Client modules
duo_lib = os.path.join(os.path.join(os.environ.get('SPLUNK_HOME')), 'etc', 'apps', 'duo-security-app', 'bin', 'lib')
if not duo_lib in sys.path:
    sys.path.append(duo_lib)
import duo_client

out_dir = os.path.join(os.path.join(os.environ.get('SPLUNK_HOME')), 'etc', 'apps', 'duo-security-app', 'bin')

from duo_to_lookup import *
lookups_dir = os.path.join(os.path.join(os.environ.get('SPLUNK_HOME')), 'etc', 'apps', 'duo-security-app', 'lookups')


logging_level = logging.DEBUG
process_name = 'duo_to_splunk'
logger = logging.getLogger(process_name)
lf = os.path.join('/var/log/splunk/', process_name + '.log')
fh = logging.handlers.RotatingFileHandler(os.environ.get('SPLUNK_HOME') + lf, maxBytes=25000000, backupCount=5)
formatter = logging.Formatter("%(asctime)-15s log_level=%(levelname)-5s process=%(name)s %(message)s")
fh.setFormatter(formatter)
logger.addHandler(fh)
logger.setLevel(logging_level)
# use this to log to stdout for console debugging
# logger.addHandler(logging.StreamHandler())


class BaseLog(object):

    def __init__(self, account, path, logname):
        self.account = account
        self.duo_conf = account['duo_conf']
        self.admin_api = account['admin_api']
        self.path = path
        self.logname = logname
        self.hostname = socket.gethostname()
        self.index = account['duo_conf']['index']
        self.sourcetype = 'duo:' + logname
        self.mintime = account['duo_conf']['delta']
        self.events = []
        self.api_host = account['api_host']
        if account['duo_conf']['debug']:
            self.log_level = logging.DEBUG
        else:
            self.log_level = logging.INFO

        # these are a list of dictionaries
        # [{'username': 'foo', 'status': 'active', 'user_id': 'nnn', ... } ...]
        self.users = self.get_lookup('users')
        self.admins = self.get_lookup('admins')

    def set_logger(self):
        logger.setLevel(self.log_level)

    def get_lookup(self, lookup_name):

        table = []
        lookup_file = os.path.join(lookups_dir, lookup_name + '.csv')
        try:
            # open file
            lf = open(lookup_file, 'r')
            logger.debug('Found %s data table found for this host: action=%s' % (lookup_name, 'open'))
        except IOError:
            # does not have CSV, so fetch it
            logger.debug('No %s data table found for this host, grabbing latest Duo lookups: action=%s' % (lookup_name, 'download'))

            if lookup_name == 'admins':
                t = Admins(self.account, lookups_dir)
                t.run()
            elif lookup_name == 'users':
                t = Users(self.account, lookups_dir)
                t.run()
            logger.debug('Finished grabbing %s data table: action=%s' % (lookup_name, 'download'))
        finally:
            with open(lookup_file, 'r') as lf:
                table = list(csv.DictReader(lf))
            logger.info('Loading lookup records: action=%s api_host=%s lookup_name=%s num_records=%s' % ('load', self.api_host, lookup_name, str(len(table))))
            return table

    def get_events(self):
        # should check for:
        # RuntimeError: Received 429 Too Many Requests
        raise NotImplementedError

    def print_events(self):

        for event in self.events:
            event_data = []

            # build additional fields from lookups
            # is_admin= ,

            for k,v in event.items():
                # put timestamp first
                if k == 'timestamp':
                    event_data.insert(0, '%s="%s"' % (k, v))

                # CIM aliases
                elif k == 'ip':
                    event_data.append('%s="%s"' % ('src_ip', v))

                elif k == 'eventtype':
                    event_data.append('%s="%s"' % ('event_type', v))

                elif k == 'device':
                    event_type.append('device="%s"' % urllib.quote(v, safe=' '))

                # admin logs have a nested field 'description' with embedded JSON
                elif k == 'description':
                    try:
                        for n, m in json.loads(v.strip('\"')).items():
                            event_data.append('%s="%s"' % (n, m))
                    except AttributeError:
                        # None Type seen
                        logger.debug('Found weird value loading nested JSON for "description": key=%s value=%s' % (k, v))
                        pass

                else:
                    event_data.append('%s="%s"' % (k, v))


            event_str = ', '.join(event_data)

            logger.debug('writing event to index: event="%s" hostname="%s" sourcetype="%s" source="%s" index="%s"' % (event_str, self.hostname,
                self.sourcetype, 'duo_to_splunk.py', self.index ) )
            input.submit( event_str, hostname = self.hostname,
                sourcetype = self.sourcetype, source = 'duo_to_splunk.py', index = self.index )

        logger.info('Successfully logged new Duo events: log_type="%s" event_count="%s"' % (self.logname, len(self.events)))


    def get_last_timestamp_path(self):
        """
        Returns the path to the file containing the timestamp of the last
        event fetched.
        """
        filename = self.logname + "_last_timestamp_" + self.admin_api.host
        path = os.path.join(self.path, filename)
        return path

    def get_mintime(self):
        """
            Updates self.mintime which is the minimum timestamp of
            log events we want to fetch.
            self.mintime is > all event timestamps we have already fetched.
        """
        try:
            # Only fetch events that come after timestamp of last event
            path = self.get_last_timestamp_path()
            self.mintime = int(open(path).read().strip()) + 1
        except IOError:
            pass

    def write_last_timestamp(self):
        """
            Store last_timestamp so that we don't fetch the same events again
        """
        if not self.events:
            # Do not update last_timestamp
            return

        last_timestamp = 0
        for event in self.events:
            last_timestamp = max(last_timestamp, event['timestamp'])

        path = self.get_last_timestamp_path()
        f = open(path, "w")
        f.write(str(last_timestamp))
        f.close()

        last_time_str = time.strftime("%Y-%m-%d %H:%M:%S %z", time.localtime(last_timestamp))

        logger.info('Marking last event timestamp found: timestamp="%s" time="%s" logtype="%s"' % (str(last_timestamp), last_time_str, self.logname))


    def run(self):
        """
        Fetch new log events and print them.
        """
        self.events = []
        self.set_logger()
        self.get_mintime()
        self.get_events()
        self.print_events()
        self.write_last_timestamp()


class AdministratorLog(BaseLog):
    def __init__(self, account, path):
        BaseLog.__init__(self, account, path, "administrator")

    def get_events(self):
        self.events = self.admin_api.get_administrator_log(
            mintime=self.mintime,
        )

    def print_events(self):

        for event in self.events:
            event_data = []

            for k,v in event.items():
                # put timestamp first
                if k == 'timestamp':
                    event_data.insert(0, '%s="%s"' % (k, v))

                # CIM aliases
                elif k == 'ip':
                    event_data.append('%s="%s"' % ('src_ip', v))

                elif k == 'eventtype':
                    event_data.append('%s="%s"' % ('event_type', v))

                # Duo somehow calls realname username here.
                elif k == 'username':
                    event_data.append('%s="%s"' % ('realname', v))

                elif k == 'device':
                    event_type.append('device="%s"' % urllib.quote(v, safe=' '))

                # admin logs may have a nested field 'description' with embedded JSON
                elif k == 'description':
                    try:
                        for n, m in json.loads(v.strip('\"')).items():
                            event_data.append('%s="%s"' % (n, m))
                    except AttributeError:
                        # None Type seen occasionally, unsure as to exact cause
                        logger.debug('Found weird value loading nested JSON for "description": key=%s value=%s' % (k, v))
                        pass
                    except ValueError:
                        # for when description is empty string
                        event_data.append('%s="%s"' % (k, v))

                else:
                    event_data.append('%s="%s"' % (k, v))


            # build additional fields from lookups
            for admin in self.admins:
                if event['username'] == admin['realname']:
                    username, domain = admin['email'].split('@')
                    event_data.append('%s="%s"' % ('username', username))

            ## to-do: add lookup for devices

            event_str = ', '.join(event_data)

            logger.debug('writing event to index: event="%s" hostname="%s" sourcetype="%s" source="%s" index="%s"' % (event_str, self.hostname,
                self.sourcetype, 'duo_to_splunk.py', self.index ) )
            input.submit( event_str, hostname = self.hostname,
                sourcetype = self.sourcetype, source = 'duo_to_splunk.py', index = self.index )

        logger.info('Successfully logged new Duo events: log_type="%s" event_count="%s"' % (self.logname, len(self.events)))


class AuthenticationLog(BaseLog):
    def __init__(self, account, path):
        BaseLog.__init__(self, account, path, "authentication")

    def get_events(self):
        self.events = self.admin_api.get_authentication_log(
            mintime=self.mintime,
        )

    def print_events(self):

        for event in self.events:
            event_data = []

            for k in sorted(event.keys()):
                # put timestamp first
                v = event[k]
                if k == 'timestamp':
                    event_data.insert(0, '%s="%s"' % (k, v))

                # CIM aliases
                elif k == 'ip':
                    event_data.append('%s="%s"' % ('src_ip', v))

                elif k == 'eventtype':
                    event_data.append('%s="%s"' % ('event_type', v))

                elif k == 'device':
                    event_type.append('device="%s"' % urllib.quote(v, safe=' '))

                # admin logs have a nested field 'description' with embedded JSON
                elif k == 'description':
                    try:
                        for n, m in json.loads(v.strip('\"')).items():
                            event_data.append('%s="%s"' % (n, m))
                    except AttributeError:
                        # None Type seen
                        logger.debug('Found weird value loading nested JSON for "description": key=%s value=%s' % (k, v))
                        pass

                else:
                    event_data.append('%s="%s"' % (k, v))


            # build additional fields from lookups
            for user in self.users:
                if event['username'] == user['username']:
                    event_data.append('%s="%s"' % ('realname', user['realname']))

            ## to-do: add lookup for is_admin, devices

            event_str = ', '.join(event_data)

            logger.debug('writing event to index: event="%s" hostname="%s" sourcetype="%s" source="%s" index="%s"' % (event_str, self.hostname,
                self.sourcetype, 'duo_to_splunk.py', self.index ) )
            input.submit( event_str, hostname = self.hostname,
                sourcetype = self.sourcetype, source = 'duo_to_splunk.py', index = self.index )

        logger.info('Successfully logged new Duo events: log_type="%s" event_count="%s"' % (self.logname, len(self.events)))


class TelephonyLog(BaseLog):
    def __init__(self, account, path):
        BaseLog.__init__(self, account, path, "telephony")

    def get_events(self):
        self.events = self.admin_api.get_telephony_log(
            mintime=self.mintime,
        )

def get_credentials(sessionKey):
    '''Given a splunk sesionKey returns a clear text user name and password from a splunk password container'''
    try:
        # list all credentials using REST API
        storage_uri = '/servicesNS/nobody/%s/storage/passwords/?output_mode=json' % ('duo-security-app')
        serverResponse, serverContent = rest.simpleRequest(storage_uri, sessionKey=sessionKey)
        storage = json.loads(serverContent)

    except Exception, e:
        import traceback
        stack =  traceback.format_exc()
        logger.warn(stack)
        raise Exception("Could not get credentials from splunk. Error: %s" % str(e))

    # return matching set of credentials
    accounts = []

    for item in storage['entry']:
        if item['acl']['app'] == 'duo-security-app':
            accounts.append({'integration_key': item['content']['username'],
                'secret_key': item['content']['clear_password'],
                'api_host': item['content']['realm'],
                })

    if accounts:
        return accounts
    else:
        # Raise an exception if no creds found
        logger.error("Problem accessing credentials")
        raise Exception("No credentials have been found")

def get_duo_config(sessionKey):
    '''  Return a duo_client.Admin object created using the parameters
    stored in the Splunk app and duo config files. '''

    # Pull config files from app
    duo_conf = entity.getEntity('/configs/conf-duo','general', namespace='duo-security-app', sessionKey=sessionKey, owner='nobody')

    # duo_conf['integration_key'], duo_conf['secret_key'], duo_conf['api_host'] = get_credentials(sessionKey)

    # support grabbing multiple accounts from app.conf
    accounts = get_credentials(sessionKey)
    duo_conf['accounts'] = []

    for account in accounts:
        admin_api = duo_client.Admin(
            ikey=account['integration_key'],
            skey=account['secret_key'],
            host=account['api_host'],
        )
        duo_conf['accounts'].append({'integration_key': account['integration_key'],
                                        'secret_key': account['secret_key'],
                                        'api_host': account['api_host'],
                                        'admin_api': admin_api,
                                        })

    return duo_conf

def main():

    logger.info('Starting to check for new Duo events: action=%s' % ('start'))

    sessionKey = ''

    # grab a sessionKey for Splunk REST access
    sessionKey = sys.stdin.readline().strip()
    splunk.setDefault('sessionKey', sessionKey)

    duo_conf = get_duo_config(sessionKey)

    if duo_conf['debug']:
        logger.setLevel(logging.DEBUG)

    for account in duo_conf['accounts']:
        ikey_masked = account['integration_key'].replace(account['integration_key'][0:13], 'xxxxxxxxxxxxxx')

        account['duo_conf'] = duo_conf

        logger.info('Fetching Duo logs with config: api_host="%s" integration="%s" delta="%s" index="%s" debug="%s"' % (account['api_host'], ikey_masked, duo_conf['delta'], duo_conf['index'], duo_conf['debug']) )

        for logclass in (AdministratorLog, AuthenticationLog, TelephonyLog):
            log = logclass(account, out_dir)
            log.run()

    logger.info('Finished checking for new Duo events: action=%s' % ('stop'))

if __name__ == '__main__':
    main()
