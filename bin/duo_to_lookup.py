#!/usr/bin/python
import ConfigParser
import optparse
import os
import sys
import time
import socket
import json
import csv
import logging
import logging.handlers
import splunk
import splunk.entity as entity
import splunk.rest as rest
import splunk.input as input


# import Duo Client modules
duo_lib = os.path.join(os.path.join(os.environ.get('SPLUNK_HOME')), 'etc', 'apps', 'duo-security-app', 'bin', 'lib')
if not duo_lib in sys.path:
    sys.path.append(duo_lib)
import duo_client

lookups_dir = os.path.join(os.path.join(os.environ.get('SPLUNK_HOME')), 'etc', 'apps', 'duo-security-app', 'lookups')


logging_level = logging.DEBUG
process_name = 'duo_to_lookup'
logger = logging.getLogger(process_name)
lf = os.path.join('/var/log/splunk/', process_name + '.log')
fh = logging.handlers.RotatingFileHandler(os.environ.get('SPLUNK_HOME') + lf, maxBytes=25000000, backupCount=5)
formatter = logging.Formatter("%(asctime)-15s log_level=%(levelname)-5s process=%(name)s %(message)s")
fh.setFormatter(formatter)
logger.addHandler(fh)
logger.setLevel(logging_level)
# use this to log to stdout for console debugging
logger.addHandler(logging.StreamHandler())


class Lookup(object):

    def __init__(self, account, path, lookup_name):
        self.duo_conf = account['duo_conf']
        self.admin_api = account['admin_api']
        self.path = path
        self.lookup_name = lookup_name
        self.hostname = socket.gethostname()
        self.api_host = account['api_host']
        self.api_name = account['api_name']
        self.records = []
        # self.ofile = None
        # self.ojson = None
        self.ocsv_path = ''
        self.ojson_path = ''

        if account['duo_conf']['debug']:
            self.log_level = logging.DEBUG
        else:
            self.log_level = logging.INFO

    def set_logger(self):
        logger.setLevel(self.log_level)

    def get_records(self):
        # should check for:
        # RuntimeError: Received 429 Too Many Requests
        raise NotImplementedError

    def get_csv(self):

        self.ocsv_path = os.path.join(self.path, self.lookup_name + '.csv')
        # try:
        #     self.ofile = open(csv_path, 'w+')
        # except IOError:
        #     logger.error('Unable to open lookup table file: lookup_name="%s" path="%s"' % (self.lookup_name, csv_path))

    def get_json(self):
        self.ojson_path = os.path.join(self.path, self.lookup_name + '_' + self.api_host + '.json')

    def write_records(self):
        raise NotImplementedError

    def run(self):
        """
        Fetch new log records and print them.
        """
        self.records = []
        self.set_logger()
        self.get_records()
        self.get_csv()
        self.get_json()
        self.write_records()


class Users(Lookup):
    def __init__(self, account, path):
        Lookup.__init__(self, account, path, 'users')

    def get_records(self):
        self.records = self.admin_api.get_users()

    def write_records(self):

        try:
            with open(self.ojson_path, 'w+') as ojson:
                json.dump(self.records, ojson)
                logger.info('Successfully wrote Duo records to JSON: api_host="%s" lookup_name="%s" record_count="%s"' % (self.api_host, self.lookup_name, len(self.records)))
        except IOError:
            logger.error('Unable to open JSON file: lookup_name="%s" path="%s"' % (self.lookup_name, self.ojson_path))

        fields = ['api_host','api_name','user_id','username','realname','email','status','groups','last_login','notes','phones','tokens']

        # determine if this is a new file or not
        existing_rows = []
        latest_rows = []
        with open(self.ocsv_path, 'r+') as ocsv:
            try:
                dialect = csv.Sniffer().has_header(ocsv.read(1024))
                ocsv.seek(0)
                csv_reader = csv.DictReader(ocsv, dialect)
                existing_rows = [row for row in csv_reader]
                logger.info('Found existing lookup file: lookup_name="%s" record_count="%s"' % (self.lookup_name, len(existing_rows)))
            except csv.Error, e:
                logger.error('Problem reading existing lookup file, new file will be created: lookup_name="%s" exception="%s"' % (self.lookup_name, str(e)))

        for row in self.records:
            row_data = {}
            for field in fields:
                # handle nested fields by consolidating down to a list of IDs
                if field == 'phones':
                    phone_ids = []
                    for phone in row[field]:
                        phone_ids.append(phone['phone_id'])
                    row_data[field] = '; '.join(phone_ids)
                elif field == 'groups':
                    group_ids = []
                    for group in row[field]:
                        group_ids.append(group['group_id'])
                    row_data[field] = '; '.join(group_ids)
                elif field == 'tokens':
                    token_ids = []
                    for token in row[field]:
                        token_ids.append(token['token_id'])
                    row_data[field] = '; '.join(token_ids)
                elif field == 'desktoptokens':
                    desktoptoken_ids = []
                    for desktoptoken in row[field]:
                        desktoptoken_ids.append(desktoptoken['desktoptoken_id'])
                    row_data[field] = '; '.join(desktoptoken_ids)
                # set where we got the data from
                elif field == 'api_host':
                    row_data['api_host'] = self.api_host
                elif field == 'api_name':
                    row_data['api_name'] = self.api_name
                else:
                    # assume normal single value, but coerce just incase for ints
                    try:
                        row_data[field] = str(row[field])
                    except TypeError:
                        logger.debug('Problem parsing JSON item: %s' % str(row))


            logger.debug('Found record with raw row_data: %s' % row_data)
            latest_rows.append(row_data)

        num_updates = 0
        num_removed = 0
        new_rows = []
        if existing_rows:
            host_rows = [erow for erow in existing_rows[1:] if erow['api_host'] == self.api_host]
            other_rows = [erow for erow in existing_rows[1:] if erow['api_host'] != self.api_host]
            new_rows.append(other_rows)
            for hrow in host_rows:
                update = {}
                for lrow in latest_rows:
                    if hrow['email'] == lrow['email']:
                        update = lrow
                        num_updates += 1
                        break
                    else:
                        num_removed += 1
                        break
                new_rows.append(update)
        else:
            new_rows = latest_rows
            num_updates = len(latest_rows)

        with open(self.ocsv_path, 'w+') as ocsv:
            csv_writer = csv.DictWriter(ocsv, fieldnames=fields, quoting=csv.QUOTE_MINIMAL)
            csv_writer.writeheader()
            csv_writer.writerows(new_rows)
            logger.info('Finished writing lookup data: api_host="%s" lookup_name="%s" total_records="%s" host_records="%s" num_updates="%s" num_removed="%s"' % (self.api_host, self.lookup_name, len(new_rows), len(self.records), num_updates, num_removed))



class Admins(Lookup):
    def __init__(self, account, path):
        Lookup.__init__(self, account, path, 'admins')

    def get_records(self):
        self.records = self.admin_api.get_admins()


    def write_records(self):

        try:
            with open(self.ojson_path, 'w+') as ojson:
                json.dump(self.records, ojson)
                logger.info('Successfully wrote Duo records to JSON: api_host="%s" lookup_name="%s" record_count="%s"' % (self.api_host, self.lookup_name, len(self.records)))
        except IOError:
            logger.error('Unable to open JSON file: lookup_name="%s" path="%s"' % (self.lookup_name, self.ojson_path))

        fields = ['api_host','api_name','admin_id','username','realname', 'email','phone','role']

        # determine if this is a new file or not
        existing_rows = []
        latest_rows = []
        with open(self.ocsv_path, 'r+') as ocsv:
            try:
                dialect = csv.Sniffer().has_header(ocsv.read(1024))
                ocsv.seek(0)
                csv_reader = csv.DictReader(ocsv, dialect)
                existing_rows = [row for row in csv_reader]
                logger.info('Found existing lookup file: lookup_name="%s" record_count="%s"' % (self.lookup_name, len(existing_rows)))
            except csv.Error, e:
                logger.error('Problem reading existing lookup file, new file will be created: lookup_name="%s" exception="%s"' % (self.lookup_name, str(e)))

        for row in self.records:
            row_data = {}
            for field in fields:
                # reassign some stupidly-named variables
                if field == 'realname':
                    row_data[field] = row['name']
                elif field == 'username':
                    username, domain = row['email'].split('@')
                    row_data[field] = username
                # set where we got the data from
                elif field == 'api_host':
                    row_data['api_host'] = self.api_host
                elif field == 'api_name':
                    row_data['api_name'] = self.api_name
                else:
                    # assume normal single value, but coerce just incase for ints
                    try:
                        row_data[field] = str(row[field])
                    except KeyError:
                        # 'role' is missing if admin roles are not enabled
                        if field == 'role':
                            row_data[field] = 'Owner'
                        else:
                            logger.debug('KeyError for item: %s' % str(row[field]))
                    except TypeError:
                        logger.debug('Problem parsing JSON item: %s' % str(row))

            logger.debug('Found record with raw row_data: %s' % row_data)
            latest_rows.append(row_data)

        num_updates = 0
        num_removed = 0
        new_rows = []
        if existing_rows:
            host_rows = [erow for erow in existing_rows[1:] if erow['api_host'] == self.api_host]
            other_rows = [erow for erow in existing_rows[1:] if erow['api_host'] != self.api_host]
            new_rows.append(other_rows)
            for hrow in host_rows:
                update = {}
                for lrow in latest_rows:
                    if hrow['email'] == lrow['email']:
                        update = lrow
                        num_updates += 1
                        break
                    else:
                        num_removed += 1
                        break
                new_rows.append(update)
        else:
            new_rows = latest_rows
            num_updates = len(latest_rows)

        with open(self.ocsv_path, 'w+') as ocsv:
            csv_writer = csv.DictWriter(ocsv, fieldnames=fields, quoting=csv.QUOTE_MINIMAL)
            csv_writer.writeheader()
            csv_writer.writerows(new_rows)
            logger.info('Finished writing lookup data: api_host="%s" lookup_name="%s" total_records="%s" host_records="%s" num_updates="%s" num_removed="%s"' % (self.api_host, self.lookup_name, len(new_rows), len(self.records), num_updates, num_removed))


def get_credentials(sessionKey):
    '''Given a splunk sesionKey returns a clear text user name and password from a splunk password container'''
    try:
        # list all credentials using REST API
        storage_uri = '/servicesNS/nobody/%s/storage/passwords/?output_mode=json' % ('duo-security-app')
        serverResponse, serverContent = rest.simpleRequest(storage_uri, sessionKey=sessionKey)
        storage = json.loads(serverContent)

    except Exception, e:
        import traceback
        stack =  traceback.format_exc()
        logger.warn(stack)
        raise Exception("Could not get credentials from splunk. Error: %s" % str(e))

    # return matching set of credentials
    accounts = []

    for item in storage['entry']:
        if item['acl']['app'] == 'duo-security-app':
            accounts.append({'integration_key': item['content']['username'],
                'secret_key': item['content']['clear_password'],
                'api_host': item['content']['realm'],
                })

    if accounts:
        return accounts
    else:
        # Raise an exception if no creds found
        logger.error("Problem accessing credentials")
        raise Exception("No credentials have been found")

def get_duo_config(sessionKey):
    '''  Return a duo_client.Admin object created using the parameters
    stored in the Splunk app and duo config files. '''

    # Pull config files from app
    duo_conf_uri = '/servicesNS/nobody/%s/configs/conf-duo/?output_mode=json' % ('duo-security-app')
    serverResponse, serverContent = rest.simpleRequest(duo_conf_uri, sessionKey=sessionKey)
    duo_conf = json.loads(serverContent)['entry'][0]['content']

    # duo_conf = entity.getEntity('/configs/conf-duo','general', namespace='duo-security-app', sessionKey=sessionKey, owner='nobody')

    # support grabbing multiple accounts from app.conf
    accounts = get_credentials(sessionKey)
    duo_conf['accounts'] = []

    for account in accounts:
        admin_api = duo_client.Admin(
            ikey=account['integration_key'],
            skey=account['secret_key'],
            host=account['api_host'],
        )
        duo_conf['accounts'].append({'integration_key': account['integration_key'],
                                        'secret_key': account['secret_key'],
                                        'api_host': account['api_host'],
                                        'admin_api': admin_api,
                                        })

    return duo_conf


def main():

    logger.info('Starting to generate Duo lookups: action=%s' % ('start'))

    sessionKey = ''

    # grab a sessionKey for Splunk REST access
    sessionKey = sys.stdin.readline().strip()
    splunk.setDefault('sessionKey', sessionKey)

    duo_conf = get_duo_config(sessionKey)

    if duo_conf['debug']:
        logger.setLevel(logging.DEBUG)

    for account in duo_conf['accounts']:
        ikey_masked = account['integration_key'].replace(account['integration_key'][0:13], 'xxxxxxxxxxxxxx')

        account['duo_conf'] = duo_conf

        account['api_name'] = ''

        try:
            account['api_name'] = [v for k,v in json.loads(duo_conf['api_names']).items() if k == account['api_host']][0]
        except Exception, e:
            import traceback
            stack =  traceback.format_exc()
            logger.warn(stack)
            raise Exception("Error: %s" % str(e))

        logger.info('Fetching Duo data with config: api_host="%s" api_name="%s" integration="%s"' % (account['api_host'], account['api_name'], ikey_masked))

        # for LookupTable in (Users, Admins, Groups, DesktopTokens, Tokens, Phones):
        for LookupTable in (Users, Admins):
            table = LookupTable(account, lookups_dir)
            table.run()

    logger.info('Finished creating Duo lookups: action=%s' % ('stop'))

if __name__ == '__main__':
    main()
