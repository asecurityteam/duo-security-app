index = <index>
	* Name of the index where the Duo log events will be written to
	* Note that internal runtime data for the script is sent to '_internal' with a sourcetype of duo_to_splunk
	* Defaults to 'duo'

delta = <delta>
	* Period, in seconds, to retrieve events for.  This is a time offset from now in seconds. * Set to 0 retrieve all messages and 900 to return the last 15 minutes
	* API limitations will be enforced by Duo - which seem to be something like 1000 events per event type, per connection.  And no more than 3 calls every minute.
	* Defaults to 1000 seconds, while scripted input runs every 900 seconds

debug = <debug>
	* Enable debug level logging
	* Defaults to 0 or False

api_names = <api_names>
	* a JSON style dictionary of API hostnames to friendly name mappings
	* if left blank, friendly name fields will be blank
